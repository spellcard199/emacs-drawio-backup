;;; emacs-drawio-backup.el --- Helpers for making a wip backup of current diagram -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Automate making backups of the current diagram in case something
;; gets lost while working on it.  `emacs-drawio-backup-start-timer'
;; is an interactive function which starts a timer that makes
;; automatic backups of the current diagram.

;;; Code:

(require 'indiumeval)
(require 'emacs-drawio-util)
(require 'emacs-drawio-global)
(require 'org-id)

(defvar emacs-drawio-backup-dir
  (expand-file-name "backup" emacs-drawio-global-user-dir))
(defvar emacs-drawio-backup-in-progress nil)
(defvar emacs-drawio-backup-index-file-name "emacs-drawio-backup-index.el")
(defvar emacs-drawio-backup-timer nil)

(defun emacs-drawio-backup-dir ()
  (file-name-as-directory (expand-file-name emacs-drawio-backup-dir)))

(defun emacs-drawio-backup-index-file ()
  (expand-file-name emacs-drawio-backup-index-file-name
                    (emacs-drawio-backup-dir)))

(defun emacs-drawio-backup--git-commit-all ()
  ;; Create dir if it doesn't exist.
  (mkdir (emacs-drawio-backup-dir) t)
  (let ((default-directory (emacs-drawio-backup-dir)))
    ;; 1. Initialize repo if not already initialized
    ;; 2. Add all files to staging area
    ;; 3. Make a new commit
    (shell-command-to-string
     (string-join
      '("([[ -d .git ]] || git init)"
        "&& git add -A"
        "&& git commit -a -m \"No comment\"")
      " "))))

(defun emacs-drawio-backup--get-current-file-data (callback)
  (indiumeval-call
   "emacsDrawio.emacsManager.getRawXmlFileDataStr"
   nil
   callback
   "emacsDrawio.emacsManager.editorUi"))

(defun emacs-drawio-backup--replace-file-contents-with-object-repr
    (obj dest-file)
  (with-temp-file dest-file
    (insert (prin1-to-string obj))))

(defun emacs-drawio-backup--index-file-read-data ()
  (when-let* ((index-file (emacs-drawio-backup-index-file))
              (index-contents
               (when (file-exists-p index-file)
                 (with-temp-buffer
                   (insert-file-contents index-file)
                   (buffer-substring-no-properties
                    (point-min) (point-max))))))
    (when (not (string-empty-p index-contents))
      (car (read-from-string index-contents)))))

(defun emacs-drawio-backup--index-file-get-add-backup-data
    (orig-file-path)
  "Get data for original file inside index file.
`emacs-drawio-backup-index-file' contains an association list with the
following shape:
  ((ORIG-FILE-PATH  orig-file-modification-time-at-last-backup-or-nil
                    backup-file-name)
   ...
  )
If ORIG-FILE-PATH is already a key in the association list: the
corresponding entry is returned.
If ORIG-FILE-PATH is not a key in the association list:
1. A new backup file name is created by calling `org-id-new' and
   appending \".xml\"
2. A new entry is added in the file pointed by
   `emacs-drawio-backup-index-file'
3. The new entry is returned.
Argument ORIG-FILE-PATH is the path of the file to backup."
  (let* ((index-data (emacs-drawio-backup--index-file-read-data))
         (index-entry (when index-data
                        (assoc orig-file-path index-data)))
         (backup-file-name
          (if index-entry
              (nth 2 index-entry)
            (concat (org-id-new) ".xml")))
         (backup-file-path (expand-file-name backup-file-name
                                             (emacs-drawio-backup-dir))))
    (when (not index-entry)
      ;; Add entry to index-data and save index file.
      (emacs-drawio-backup--replace-file-contents-with-object-repr
       (cons (setq index-entry
                   (list orig-file-path
                         :nil-placeholder
                         backup-file-name))
             index-data)
       (emacs-drawio-backup-index-file)))
    index-entry))

(defun emacs-drawio-backup--index-update-modification-time
    (orig-file-path)
  (let* ((index-data (emacs-drawio-backup--index-file-read-data))
         (entry (assoc orig-file-path index-data))
         (entry-modification-time (nth 1 entry))
         (actual-modification-time
          (file-attribute-modification-time
           (file-attributes orig-file-path))))
    (when (not (equal entry-modification-time
                      actual-modification-time))
      (setf (cadr (assoc orig-file-path index-data))
            actual-modification-time)
      (emacs-drawio-backup--replace-file-contents-with-object-repr
       index-data (emacs-drawio-backup-index-file)))))

(defun emacs-drawio-backup--write-backup-callback
    (iro--current-file-data)
  (when-let* ((current-file-data-as-string
               (string-trim
                (indium-remote-object-to-string
                 iro--current-file-data)
                "\"" "\""))
              (current-file-data
               (when (not (string-equal
                           current-file-data-as-string
                           "null"))
                 (json-read-from-string
                  current-file-data-as-string)))
              (orig-file-path
               (cdr (assoc 'path current-file-data)))
              (file-contents
               (cdr (assoc 'data current-file-data)))
              (index-entry
               (emacs-drawio-backup--index-file-get-add-backup-data
                orig-file-path))
              (backup-file-name
               (nth 2 index-entry))
              (backup-file-path
               (expand-file-name backup-file-name
                                 (emacs-drawio-backup-dir)))
              (orig-file-modification-time-index
               (nth 1 index-entry))
              (orig-file-modification-time-now
               (file-attribute-modification-time
                (file-attributes orig-file-path))))
    (when (not (equal orig-file-modification-time-index
                      orig-file-modification-time-now))
      (with-temp-file backup-file-path
        (insert file-contents))
      (emacs-drawio-backup--index-update-modification-time
       orig-file-path)
      (emacs-drawio-backup--git-commit-all)
      (message (format "[emacs-drawio-backup] Backupped at: %s" backup-file-path)))))

(defun emacs-drawio-backup--write-backup ()
  (when (not emacs-drawio-backup-in-progress)
    (setq emacs-drawio-backup-in-progress t)
    (unwind-protect
        (emacs-drawio-backup--get-current-file-data
         #'emacs-drawio-backup--write-backup-callback)
      (setq emacs-drawio-backup-in-progress nil))))

(defun emacs-drawio-backup-stop-timer ()
  (interactive)
  (when emacs-drawio-backup-timer
    (cancel-timer emacs-drawio-backup-timer)))

(defun emacs-drawio-backup-start-timer ()
  (interactive)
  (emacs-drawio-backup-stop-timer)
  (setq emacs-drawio-backup-timer
        (run-with-idle-timer 10 t #'emacs-drawio-backup--write-backup)))

(defun emacs-drawio-backup-delete-backup-dir ()
  (interactive)
  (delete-directory (emacs-drawio-backup-dir) t))

(provide 'emacs-drawio-backup)
;;; emacs-drawio-backup.el ends here
